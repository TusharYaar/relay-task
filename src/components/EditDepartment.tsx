import React from 'react'
import { FormContainer } from '@saastack/layouts/containers'

import EditForm from '../forms/EditForm'

import { DepartmentInput } from '../__generated__/CreateDepartmentMutation.graphql'

type Props = {
    onClose: () => void
    onSubmit: (type: 'NEW' | 'UPDATE', department: any) => void
    type: 'NEW' | 'UPDATE'
    initialValues: DepartmentInput
    loading: boolean
}

const EditDepartment = ({ onClose, type, onSubmit, initialValues, loading }: Props) => {
    const handleSubmit = (input: DepartmentInput) => {
        onSubmit(type, input)
    }
    return (
        <FormContainer
            open
            formId="edit-dept-form"
            onClose={onClose}
            header={`${type === 'NEW' ? 'Add' : 'Update'} Department`}
            loading={loading}
        >
            <EditForm
                type={type}
                id="edit-dept-form"
                onSubmit={handleSubmit}
                initialValues={initialValues}
            />
        </FormContainer>
    )
}

export default EditDepartment
