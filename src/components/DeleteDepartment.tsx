import React from 'react'
import { ConfirmContainer } from '@saastack/layouts/containers'

type Props = {
    onClose: () => void
    handleDelete: () => void
    loading: boolean
}

const DeleteDepartment = ({ onClose, handleDelete, loading }: Props) => {
    return (
        <ConfirmContainer
            header="Delete Department"
            open
            onClose={onClose}
            onAction={handleDelete}
            loading={loading}
        />
    )
}

export default DeleteDepartment
