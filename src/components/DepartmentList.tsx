import React from 'react'

import { createFragmentContainer } from 'react-relay'
import { graphql } from 'relay-runtime'

import { useAlert } from '@saastack/core'

import {
    IconButton,
    List,
    ListItem,
    ListItemAvatar,
    ListItemSecondaryAction,
    ListItemText,
    Tooltip,
} from '@material-ui/core'
import { DeleteOutlined } from '@material-ui/icons'
import { makeStyles } from '@material-ui/styles'

import { Avatar } from '@saastack/components'

import { DepartmentList_departments } from '../__generated__/DepartmentList_departments.graphql'

type Props = {
    departments: DepartmentList_departments
    onClick(action: 'UPDATE' | 'NEW', department: any): void
    onDelete: (id: string) => void
}

const useStyles = makeStyles({
    list: {
        '& .MuiListItemSecondaryAction-root': {
            visibility: 'hidden',
        },
        '& li:hover': {
            '& .MuiListItemSecondaryAction-root': {
                visibility: 'visible',
            },
        },
    },
})

const DepartmentList = ({ departments, onClick, onDelete }: Props) => {
    const classes = useStyles()

    return (
        <List className={classes.list}>
            {departments.map((dept) => (
                <ListItem key={dept.id} divider button onClick={() => onClick('UPDATE', dept)}>
                    <ListItemAvatar>
                        <Avatar title={dept.name} />
                    </ListItemAvatar>
                    <ListItemText primary={dept.name} secondary={dept.description} />
                    <ListItemSecondaryAction>
                        <Tooltip title={`Delete ${dept.name}`}>
                            <IconButton>
                                <DeleteOutlined onClick={() => onDelete(dept.id)} />
                            </IconButton>
                        </Tooltip>
                    </ListItemSecondaryAction>
                </ListItem>
            ))}
        </List>
    )
}

export default createFragmentContainer(DepartmentList, {
    departments: graphql`
        fragment DepartmentList_departments on Department @relay(plural: true) {
            id
            name
            description
        }
    `,
})
