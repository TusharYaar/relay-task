import React, { useState, useEffect } from 'react'

import { graphql } from 'react-relay'
import { useRelayEnvironment } from 'react-relay/hooks'

import { useConfig } from '@saastack/core'
import { ErrorComponent, Loading } from '@saastack/components'
import { fetchQuery } from '@saastack/relay'
import DepartmentMaster from '../components/DepartmentMaster'

import {
    DepartmentPageQueryResponse,
    DepartmentPageQueryVariables,
    DepartmentPageQuery,
} from '../__generated__/DepartmentMaster_departments.graphql'

const query = graphql`
    query DepartmentPageQuery($parent: String) {
        ...DepartmentMaster_departments @arguments(parent: $parent)
    }
`

const DepartmentPage = () => {
    const { companyId } = useConfig()
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState<any>(null)
    const environment = useRelayEnvironment()
    const dataRef = React.useRef<DepartmentPageQueryResponse | null>(null)
    const fetchDepartments = () => {
        const variables: DepartmentPageQueryVariables = {
            parent: companyId,
        }
        fetchQuery<DepartmentPageQuery>(environment, query, variables, { force: true })
            .then((resp) => {
                dataRef.current = resp
                console.log(resp)
                setLoading(false)
            })
            .catch((error) => {
                setError(error)
                setLoading(false)
            })
    }
    useEffect(fetchDepartments, [])

    if (loading) {
        return <Loading />
    }
    if (error) {
        return <ErrorComponent error={error} />
    }

    return (
        <DepartmentMaster
            parent={companyId}
            departments={dataRef.current}
            refetch={fetchDepartments}
        />
    )
}

export default DepartmentPage
