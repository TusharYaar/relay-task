import React from 'react'
import DesignationPage from '../pages/DepartmentPage'
import Wrapper from './Wrapper'

export default {
    title: 'Relay Task',
    decorators: [(storyFn: () => JSX.Element) => <Wrapper>{storyFn()}</Wrapper>],
}

export const Default = () => <DesignationPage />
