import { Mutable, TemplateLoader } from '@saastack/core'
import { NetworkPolicyProvider, useQuery, withEnvironment } from '@saastack/relay'
import { ErrorComponent, Loading } from '@saastack/components'
import { Router } from '@saastack/router'
import React from 'react'
import { graphql } from 'relay-runtime'
import { WrapperQuery } from '../__generated__/WrapperQuery.graphql'

const query = graphql`
    query WrapperQuery {
        viewer {
            id
            firstName
            lastName
            email
            profileImage {
                thumbImage
            }
            preferences {
                dateFormat
                language
                timeFormat
                timezone
                uiInfo
            }
            userRoles {
                role {
                    levelDetails {
                        ... on Location {
                            id
                        }
                        ... on Company {
                            id
                        }
                        ... on Group {
                            id
                        }
                        ... on Employee {
                            id
                        }
                    }
                    levelId
                    roleId
                    role {
                        id
                        roleName
                        isDefault
                        priority
                        level
                        moduleRoles {
                            name
                        }
                    }
                }
            }
            groups {
                id
                name
                companies {
                    id
                    title
                    displayName
                    metadata
                    gallery {
                        default {
                            thumbImage
                        }
                    }
                    address {
                        country
                    }
                    companySettings {
                        navMenus
                        aliases(locale: "en-US")
                    }
                    apps {
                        id
                        appTypeId
                        name
                        active
                        serviceModules
                    }
                    locations(first: 500) {
                        edges {
                            node {
                                id
                                name
                                preference {
                                    currency
                                }
                                active
                            }
                        }
                    }
                    licenseWallet {
                        license {
                            moduleLicenses {
                                enabled
                                freeLimit
                                licenseLimit
                                maxAddonLimit
                                maxLimit
                                moduleName
                                paidAddonLimit
                                restrictionSlugs
                                restrictionType
                                rpcs
                                slug
                                totalLimit
                            }
                        }
                    }
                }
            }
        }
    }
`

const Wrapper: React.FC = (props) => {
    const { data } = useQuery<WrapperQuery>(query)
    if (!data) {
        return <>Loading</>
    }
    const employeeIds =
        data?.viewer?.userRoles?.role
            ?.filter?.((role) => role?.role?.level === 'emp')
            ?.map((role) => role.levelId) ?? []
    return (
        <NetworkPolicyProvider fetchPolicy="network-only" retain={true}>
            <TemplateLoader
                getLocale={() => Promise.resolve({ messages: {} })}
                viewer={data?.viewer as Mutable<WrapperQuery['response']['viewer']>}
                employeeIds={employeeIds}
            >
                <Router>{props.children}</Router>
            </TemplateLoader>
        </NetworkPolicyProvider>
    )
}

export default withEnvironment({ graphqlUrl: process.env.REACT_APP_GRAPHQL_URL })(Wrapper)
